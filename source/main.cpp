

#include <pspp/modern_main.hpp>
#include <pspp/io.hpp>

struct user_input_token
{
  enum struct type_t
  { declaration, string };

  type_t type {};

};

struct user_input
{
  std::string_view str;
};

template <typename CharT>
struct basic_winux_path_view
{
  struct iterator
  {
    CharT const * m_ptr;

    constexpr char operator*() const noexcept
    {
      if (*m_ptr == '\\')
        return '/';
      else return *m_ptr;
    }

    constexpr auto operator<=>(iterator const & other) const noexcept = default;

    constexpr iterator & operator++() noexcept
    { ++m_ptr; return *this; }

    constexpr iterator operator++(int) noexcept
    {
      iterator prev { m_ptr };
      ++m_ptr;
      return prev;
    }

    constexpr iterator & operator--() noexcept
    { --m_ptr; return *this; }

    constexpr iterator operator--(int) noexcept
    {
      iterator prev { m_ptr };
      --m_ptr;
      return prev;
    }
  };

  using string_t = std::basic_string_view<CharT>;

  string_t raw_str;

  constexpr iterator begin() const noexcept
  { return { raw_str.begin() }; }

  constexpr iterator end() const noexcept
  { return { raw_str.end() }; }

  friend
  constexpr std::ostream & operator<<(std::ostream & o, basic_winux_path_view str) noexcept
  {
    for ( auto const c : str )
      o << c;

    return o;
  }
};

using winux_path_view = basic_winux_path_view<std::filesystem::path::value_type>;


struct line_prefix_format
{};

struct console
{
  inline static constinit std::string input_buffer {};

      void
  output_prefix
    ( [[maybe_unused]] line_prefix_format format)
      noexcept
      {
  pspp::print("{} > ", std::filesystem::current_path().c_str());
      }

      std::string_view
  get_user_input()
      noexcept
      {
        input_buffer.clear();
        output_prefix( {} );

        std::getline(std::cin, input_buffer);
        while (input_buffer.back() == '\\')
        {
          pspp::print("... ");
          std::getline(std::cin, input_buffer);
        }

        return input_buffer;
      }
};


void true_main() noexcept
{
  pspp::println("winux 0.0.0.0.0.0.0.1");

  console c {};

  while ( true )
  {
    std::string_view input = c.get_user_input();
    pspp::println("<< got: `{}`", input);
    if (input == ":exit")
      break;
  }
}



PSPP_TRUE_MAIN_ENTRY()
